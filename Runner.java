import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Runner {
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader("file.txt"));
		BufferedWriter out = new BufferedWriter(new FileWriter("file2.txt"));
		String line;
		int counter = 29;
		try {
			while ((line = in.readLine()) != null)   {
				int firstComma = line.indexOf(",");
				int secondComma = line.indexOf(",",firstComma+1);
				String college = line.substring(0,firstComma);
				String latitude = line.substring(firstComma+1,secondComma);
				String longitude = line.substring(secondComma+1);
				System.out.println("<option value=\"" + college +"\">" + college + "</option>");
				out.write("<option value=\"" + college +"\">" + college + "</option>" + "\n");
				counter++;
			}
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
